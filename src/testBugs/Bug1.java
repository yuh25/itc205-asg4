package testBugs;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import crownAndAnchorGame.Dice;
import crownAndAnchorGame.DiceValue;
import crownAndAnchorGame.Game;
import crownAndAnchorGame.Player;

//	Bug 1: Game does not pay out at correct level.
//	When player wins on 1 match, balance does not increase.

public class Bug1 {

    Dice d1;
    Dice d2;
    Dice d3;
	
    Player player;
    Game game;    
    
	@Before
	public void setUp() throws Exception {
		d1 = new Dice();
		d2 = new Dice();
		d3 = new Dice();
		game = new Game(d1, d2, d3);
		player = new Player("Fred", 100);		
	}


	@Test
	public void testGame() {
		
	    int initialBalance = player.getBalance();
	    
	    // set the pick to the first die, to make sure the player wins the match.
	    DiceValue pick = d1.getValue();	
	    
	    // execute - start a match.
	    int winnings = game.playRound(player, pick, 5);
	    
	    // This tests whether the players balance has increased or not.
	    assertTrue(player.getBalance() == (initialBalance + winnings));	    
	}
}
