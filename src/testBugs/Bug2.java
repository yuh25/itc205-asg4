package testBugs;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import crownAndAnchorGame.Dice;
import crownAndAnchorGame.DiceValue;
import crownAndAnchorGame.Game;
import crownAndAnchorGame.Player;

//	Bug 2: Player cannot reach betting limit:
//	Limit set to 0, but game ends with player still with 5 (dollars) remaining.

public class Bug2 {
	
    Dice d1;	
    Player player;
    Game game;    
    
	

	@Before
	public void setUp() throws Exception {
		d1 = new Dice();
		game = new Game(d1, d1, d1);
		player = new Player("Fred", 50);// player starts with $50
		player.setLimit(0);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		int bet = 5;
		
		//initialise the pick to a random value.
		DiceValue pick = DiceValue.getRandom();
		
		// set the value to a value d1 isn't to make sure the player loses.
		while(pick == d1.getValue()){
			pick = DiceValue.getRandom();
		}
		
		// keep playing rounds until the player has reached the limit, $0
		while(player.balanceExceedsLimitBy(bet)){
			game.playRound(player, pick, bet);
		}
		
		// check that the players balance is $0
		System.out.println("Player finishes with $" + player.getBalance());
		assertTrue(player.getBalance()==0);
	}

}
