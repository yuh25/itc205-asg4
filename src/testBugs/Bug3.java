package testBugs;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import crownAndAnchorGame.Dice;
import crownAndAnchorGame.DiceValue;
import crownAndAnchorGame.Game;
import crownAndAnchorGame.Player;

//	Bug 1: Odds in the game do not appear to be correct.

//	Test runs through 100,000 rounds and checks whether the odds are approximately equal 0.42

public class Bug3 {

    Dice d1,d2,d3;	
    Player player;
    Game game;    
    
	@Before
	public void setUp() throws Exception {
		d1 = new Dice();
		d2 = new Dice();
		d3 = new Dice();
		game = new Game(d1, d2, d3);
		
	}

	@Test
	public void testGame() {
		
		double ratio = 0;
		int winCount = 0;
		int loseCount = 0; 
		//	run through 100,000 rounds
		for (int i = 0; i < 100000; i++) {
			
			// make sure the player has enough money to bet
			player = new Player("Fred",10);
			player.setLimit(0);
			
			// create a random pick.
			DiceValue pick = DiceValue.getRandom();
			
			// play a round and check whether the player won or lost.
			if (game.playRound(player, pick, 5)>0) {
				winCount++;
			} else {
				loseCount++;
			}
		}
		// calculate the odds for the games played
		ratio = (double) winCount/(winCount+loseCount);

		// check that the ratios are correct
		System.out.println("Ratio: "+ratio+" Round wins: "+ winCount+"/100000" );
		assertTrue(ratio>0.41 && ratio<0.43);
	}
	
	
	@Test
	public void testDiceValuesGetRandom() {
		int[] dieValueResults = new int[6];
		float[] dieOdds = new float[6];
		int numRolls = 100000;
		for(int i = 0;i<numRolls;i++){
			DiceValue dieRoll = DiceValue.getRandom();
			switch(dieRoll){			
			case CROWN:
				dieValueResults[0]++;
				break;
			case ANCHOR:
				dieValueResults[1]++;
				break;
			case HEART:
				dieValueResults[2]++;
				break;
			case DIAMOND:
				dieValueResults[3]++;
				break;
			case CLUB:
				dieValueResults[4]++;
				break;
			case SPADE:
				dieValueResults[5]++;
				break;			
			}
		}
		
		for(int i=0;i<6;i++){
			dieOdds[i] = (float)dieValueResults[i]/numRolls;
		}			
		
		System.out.println("CROWN:"+	dieOdds[0]*100+"%");
		System.out.println("ANCHOR:"+	dieOdds[1]*100+"%");
		System.out.println("HEART:"+	dieOdds[2]*100+"%");
		System.out.println("DIAMOND:"+	dieOdds[3]*100+"%");
		System.out.println("CLUB:"+		dieOdds[4]*100+"%");
		System.out.println("SPADE:"+	dieOdds[5]*100+"%");
		
		for(int i=0;i<6;i++){
			assertTrue(dieOdds[i]>0.15 && dieOdds[i]<0.17);
		}	
		
		
	}
	
}
