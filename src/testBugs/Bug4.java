package testBugs;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import crownAndAnchorGame.Dice;
import crownAndAnchorGame.DiceValue;
import crownAndAnchorGame.Game;
import crownAndAnchorGame.Player;

//	Bug 4: Each dice roll is the same as the previous round.

//	Test runs through 100 rounds to check if the dice rolls changes.

public class Bug4 {

    Dice d1,d2,d3;	
    Player player;
    Game game;    
    
	@Before
	public void setUp() throws Exception {
		d1 = new Dice();
		d2 = new Dice();
		d3 = new Dice();
		game = new Game(d1, d2, d3);		
	}

	@Test
	public void testGame() {

		DiceValue initValues[] = new DiceValue[3];
		List<DiceValue> cdv;
		boolean valuesChanged = false;
		
		//	make sure the player has enough money to bet
		player = new Player("Fred",10);
		
		//	create a random pick
		DiceValue pick = DiceValue.getRandom();
		game.playRound(player, pick, 5);
		cdv = game.getDiceValues();
		
		//	set the initValues to the first roll of the game.
		initValues[0] = cdv.get(0);
		initValues[1] = cdv.get(1);
		initValues[2] = cdv.get(2);
		
		//	run through 100 rounds
		for (int i = 0; i < 100; i++) {			
			//	make sure the player has enough money to bet
			player = new Player("Fred",10);
			
			// create a random pick.
			pick = DiceValue.getRandom();
			game.playRound(player, pick, 5);
			cdv = game.getDiceValues();
			
			// check if any of the rolls changed the die values.
			if(initValues[0] != cdv.get(0) || initValues[1] != cdv.get(1) || initValues[2] != cdv.get(2)){
				valuesChanged = true;
			}
		}	
		System.out.println("Initial Values: "+initValues[0]+" "+initValues[1]+" "+initValues[2]);
		System.out.println("Final Values: " + cdv.toString());
		assertTrue(valuesChanged);
	}
	
}
